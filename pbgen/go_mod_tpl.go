package pbgen

const GO_MOD_TPL = `module {{ .Vcs }}

go 1.15

require (
	google.golang.org/grpc v1.35.0
	google.golang.org/protobuf v1.25.0
)
`

type GoModTemplateData struct {
	Pkg *PbPackage
	Vcs string
}