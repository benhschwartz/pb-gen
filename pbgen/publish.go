package pbgen

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"path"
)


func GeneratePublishScript(pkg *PbPackage, buildPb string, buildVcs string, branch string, buildNum string) error {
	script := []byte("#!/usr/bin/env bash\n\n")

	for _, lang := range pkg.Config.Languages {
		langScript, err := generatePublishScriptForLang(pkg, lang, buildPb, buildVcs, branch, buildNum)
		if err != nil {
			return err
		}
		script = append(script, langScript...)
	}
	if err := ioutil.WriteFile(path.Join(buildPb, pkg.Name, "publish.sh"), script, 0777); err != nil {
		return err
	}

	return nil
}
func generatePublishScriptForLang(pkg *PbPackage, lang PbLang, buildPb string, buildVcs string, branch string, buildNum string) ([]byte, error) {


	data := &PublishTemplateData{
		GitAddr:      lang.Vcs,
		SrcPath:      path.Join(buildPb, pkg.Name, lang.Type),
		RepoPath:     path.Join(buildVcs, pkg.Name+"-"+lang.Type),
		TargetBranch: branch,
		TagBuild: "false",
	}
	if buildNum != "local" && branch == "master" {
		data.TagBuild = "true"
		data.Version = fmt.Sprintf("v%s.%s", pkg.Config.VersionBase, buildNum)
	}

	var tpl bytes.Buffer

	t, err := template.New("").Parse(GIT_PUBLISH_TPL)
	if err != nil {
		return  nil, err
	}

	if err := t.Execute(&tpl, data); err != nil {
		return nil, err
	}

	return tpl.Bytes(), nil
}
