package pbgen

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path"
)

type PbLang struct {
	Type string `yaml:"type"`
	Vcs  string `yaml:"vcs"`
}

type PbPackageConfig struct {
	Languages   []PbLang `yaml:"languages"`
	VersionBase string   `yaml:"version_base"`
}

func CopySrcConfig(srcDir string, pkgName string, buildDir string) (*PbPackageConfig, error) {
	pkgDir := path.Join(srcDir, pkgName)
	cfgPath := path.Join(pkgDir, "config.yaml")

	if _, err := os.Stat(cfgPath); os.IsNotExist(err) {
		return nil, fmt.Errorf("Package %s missing config.yaml", pkgName)
	}
	fmt.Printf(" > Config found at %s\n", cfgPath)

	cfg := &PbPackageConfig{}

	yamlFile, err := ioutil.ReadFile(cfgPath)
	if err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(yamlFile, cfg); err != nil {
		return nil, err
	}

	d, err := yaml.Marshal(cfg)
	if err != nil {
		return nil, err
	}
	outPath := path.Join(buildDir, pkgName, "config.yaml")

	buildDest := path.Join(buildDir, pkgName)
	if err := os.MkdirAll(buildDest, os.ModePerm); err != nil {
		return nil, err
	}

	if err = ioutil.WriteFile(outPath, d, 0644); err != nil {
		return nil, err
	}

	return cfg, nil
}
