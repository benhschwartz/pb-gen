package pbgen

import (
	"bytes"
	"fmt"
	"html/template"
	"os"
	"os/exec"
	"path"
)

func GenerateProtobuf(p *PbPackage, buildDir string, srcPath string, branch string, buildNum string) error {

	for _, lang := range p.Config.Languages {
		buildDest := path.Join(buildDir, p.Name, lang.Type)
		if err := os.MkdirAll(buildDest, os.ModePerm); err != nil {
			return err
		}

		switch lang.Type {
		case "go":
			if err := generateGo(p, lang.Vcs, srcPath, buildDest, branch, buildNum); err != nil {
				return err
			}
		case "python":
			if err := generatePython(p, lang.Vcs, srcPath, buildDest, branch, buildNum); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unknown lang %s", lang.Type)
		}

	}
	return nil
}

func generateGo(p *PbPackage, vcs string, srcPath string, buildDest string, branch string, buildNum string) error {
	// create a go.mod file
	data := &GoModTemplateData{
		Pkg: p,
		Vcs: vcs,
	}
	if err := createFileFromTpl(path.Join(buildDest, "go.mod"), GO_MOD_TPL, data); err != nil {
		return err
	}

	// generate protobuf code
	cmdStr := fmt.Sprintf("protoc -I=%s --go_out=plugins=grpc:%s %s/*.proto", srcPath, buildDest, srcPath)
	fmt.Println(cmdStr)
	if err := runCommand(cmdStr); err != nil {
		return err
	}

	return nil
}
func generatePython(p *PbPackage, vcs string, srcPath string, buildDest string, branch string, buildNum string) error {
	// generate python code to package folder in repo
	buildDestPkgPath := path.Join(buildDest, p.Name)
	if err := os.MkdirAll(buildDestPkgPath, os.ModePerm); err != nil {
		return err
	}

	// create an empty __init__.py file
	if err := createFileFromTpl(path.Join(buildDestPkgPath, "__init__.py"), PYTHON_INIT_TPL, p); err != nil {
		return err
	}

	// create a setup.py file
	data := PythonSetupTemplateData{
		Pkg: p,
		Vcs: vcs,
	}
	if buildNum != "local" {
		if branch == "master" {
			data.Version = fmt.Sprintf("%s.%s", p.Config.VersionBase, buildNum)
		} else {
			data.Version = fmt.Sprintf("%s.%s-%s%s", p.Config.VersionBase, buildNum, branch, buildNum)
		}
	} else {
		data.Version = "0.0.0-local"
	}

	if err := createFileFromTpl(path.Join(buildDest, "setup.py"), PYTHON_SETUP_TPL, data); err != nil {
		return err
	}

	// generate protobuf code
	cmdStr := fmt.Sprintf("protoc -I=%s --python_out=%s %s/*.proto", srcPath, buildDestPkgPath, srcPath)
	fmt.Println(cmdStr)
	if err := runCommand(cmdStr); err != nil {
		return err
	}

	return nil
}

func createFileFromTpl(destPath string, tpl string, data interface{}) error {
	f, err := os.Create(destPath)
	if err != nil {
		return err
	}
	defer f.Close()

	t, err := template.New("").Parse(tpl)
	if err != nil {
		return err
	}
	if err := t.Execute(f, data); err != nil {
		return err
	}
	return nil
}

func runCommand(cmdStr string) error {
	cmd := exec.Command("sh", "-c", cmdStr)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("Exec Error: %s\n", stderr.String())
	}
	fmt.Printf("Result: %s\n", out.String())
	return nil
}