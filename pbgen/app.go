package pbgen

import (
	"fmt"
	"path"
)

const BUILD_NS_GENERATED_CODE = "pb"
const BUILD_NS_VCS_CLONE = "vcs"

type PbPackage struct {
	Name   string
	Config *PbPackageConfig
}

type PbGen struct {
	SrcDir    string
	BuildRoot string
	BuildPb   string
	BuildVcs  string
}

func New(srcDir string, buildRoot string) *PbGen {
	return &PbGen{
		SrcDir:    srcDir,
		BuildRoot: buildRoot,
		BuildPb:   path.Join(buildRoot, BUILD_NS_GENERATED_CODE),
		BuildVcs:  path.Join(buildRoot, BUILD_NS_VCS_CLONE),
	}
}

func (p *PbGen) Generate(pkgName string, branch string, buildNum string) error {
	fmt.Printf("Finding Config for %s\n", pkgName)
	config, err := CopySrcConfig(p.SrcDir, pkgName, p.BuildPb)
	if err != nil {
		return err
	}

	pkg := &PbPackage{
		Name:   pkgName,
		Config: config,
	}

	fmt.Printf("Generating protobuf for %s\n", pkgName)
	if err := GenerateProtobuf(pkg, p.BuildPb, path.Join(p.SrcDir, pkgName), branch, buildNum); err != nil {
		return err
	}

	fmt.Printf("Generating publish script for %s\n", pkgName)

	if err := GeneratePublishScript(pkg, p.BuildPb, p.BuildVcs, branch, buildNum); err != nil {
		return err
	}


	return nil
}
