package pbgen

const PYTHON_INIT_TPL = ""

const PYTHON_SETUP_TPL = `
from setuptools import setup

setup(
    name='{{ .Pkg.Name }}',
    version='{{ .Version }}',
    description='Generated protobuf package for {{ .Pkg.Name }}',
    url='https://{{ .Vcs }}',
    author='Shippo',
    author_email='circleci@goshippo.com',
    license='unlicense',
    packages=['{{ .Pkg.Name }}'],
    zip_safe=False
)
`

type PythonSetupTemplateData struct {
	Pkg *PbPackage
	Vcs string
	Version string
}