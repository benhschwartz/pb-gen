package pbgen

const GIT_PUBLISH_TPL = `

git clone https://{{ .GitAddr }} {{ .RepoPath }}

cd "{{ .RepoPath }}"

if ! git show-branch {{ .TargetBranch }}; then
	git branch {{ .TargetBranch }}
fi

git checkout {{ .TargetBranch }}

if git ls-remote --heads --exit-code origin {{ .TargetBranch }}; then
	git pull origin {{ .TargetBranch }}
fi

cp -R {{ .SrcPath }}/* {{ .RepoPath }}/

git add -N .

if ! git diff --exit-code > /dev/null; then
	git add .
	git commit -m "Auto Creation of Proto"
	git push origin HEAD

	if [ "{{ .TagBuild }}" = "true" ]; then
		git tag {{ .Version }}
		git push origin --tags
	fi

else
	echo "No changes detected"
fi
`
type PublishTemplateData struct {
	GitAddr      string
	SrcPath      string
	RepoPath     string
	TargetBranch string
	TagBuild string
	Version string
}
