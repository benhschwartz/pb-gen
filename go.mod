module bitbucket.org/benhschwartz/pb-gen

go 1.15

require (
	github.com/golang/protobuf v1.4.1
	github.com/spf13/viper v1.7.1
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.2.4
)
