#!/usr/bin/env bash
# This script is meant to build and compile every protocolbuffer for each
# service declared in this repository (as defined by sub-directories).
# It compiles using docker containers based on Namely's protoc image
# seen here: https://github.com/namely/docker-protoc

set -e

GIT_URL_BASE="git@bitbucket.org:benhschwartz"

BUILD_ROOT=$(pwd)/build/pb/
API_ROOT=$(pwd)/api


function generate {
	srcDir=$1
	destDir=$2
	service=$3
	language=$4

	echo "    ${srcDir} -> ${destDir}"
	mkdir -p $destDir
	docker run -v $srcDir:/defs -v $destDir/:/out namely/protoc-all -f *.proto -l $language -o /out

	echo "module bitbucket.org/benhschwartz/protorepo-${service}-go" > "${destDir}go.mod"
	echo "" >> "${destDir}go.mod"
	echo "go 1.15" >> "${destDir}go.mod"
	echo "" >> "${destDir}go.mod"

	# recurse sub dirs
	for subDir in $(ls -d $srcDir/*/ 2>/dev/null); do
		subFolder=$(basename $subDir)
		generate "${srcDir}${subFolder}/" "${destDir}${subFolder}/" $service $language
	done
}


function generateAll {
	rm -rf build/
	# generate for each service directory in repo root
	for serviceDir in $API_ROOT/*/; do
		echo $serviceDir
		if [ -f $serviceDir/.protolangs ]; then
			service=$(basename ${serviceDir%/})

			echo "Generating for ${service}:"

			# generate for each language
			while read language; do
				echo "  ${language}:"

				srcDir="${API_ROOT}/${service}/"
				destDir="${BUILD_ROOT}/${service}/${language}/"
				generate $srcDir $destDir $service $language
			done < $serviceDir/.protolangs
		fi
	done
}

generateAll