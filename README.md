## pb-gen

Utility to generate and publish protobuf files to git repos in an opinionated way



### Project Layout
A project leveraging `pb-gen` should follow the following conventions
#### Structure

In an `api` folder, place each protobuf package you want to generate & publish

    ├── api
    │   └── zoo
    │       ├── config.yaml
    │       └── zoo.proto
    ├── go.mod
    └── main.go

#### config.yaml

Create a `config.yaml` in each `./api/NAME` protobuf package folder:
- languages to generate each package for (as well as target git location they should be published to)
- version_base with `major.minor` to define the version of your package should be published as (patch should be generated by ci)

example:

    languages:
    - type: go
      vcs: bitbucket.org/benhschwartz/protorepo-zoo-go
    - type: python
      vcs: bitbucket.org/benhschwartz/protorepo-zoo-python
    version_base: 0.3


### Dependencies

- protoc: pb-gen calls protoc to generate code 
- git: pb-gen uses git to publish packages

### Usage

#### Generate
Generate protobuf code for all configured languages to a build directory:

    $ pb-gen generate zoo
      ...
    $ tree build
    build
    └── pb
        └── zoo
            ├── config.yaml
            ├── go
            │   ├── go.mod
            │   └── zoo.pb.go
            ├── python
            │   ├── setup.py
            │   └── zoo
            │       ├── __init__.py
            │       └── zoo_pb2.py
            └── publish.sh

This can be leveraged locally with a `go.mod` replacement

    	go mod edit -replace bitbucket.org/benhschwartz/protorepo-zoo-go=$(pwd)/build/pb/zoo/go
#### Publish
Publish the generated code to the configured git repo with the generated `publish.sh`:

    ./build/pb/zoo/publish.sh

(This should only be a CI step and not run as part of the local dev sdlc)


### CircleCI

This command is integrated into a [CircleCI ORB](https://circleci.com/developer/orbs/orb/benschw/protoc-tools-orb)
to make generating and publishing your protobuf packages easy

