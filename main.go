package main

import (
	"bitbucket.org/benhschwartz/pb-gen/pbgen"
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

func init() {
	viper.AutomaticEnv()
	viper.SetDefault("SRC_DIR", "./api")
	viper.SetDefault("BUILD_DIR", "./build")
	viper.SetDefault("CIRCLE_BRANCH", "branch-not-available")
	viper.SetDefault("CIRCLE_BUILD_NUM", "local")
}

func usage() {
	fmt.Printf("Usage: pb-gen <pkg_name>\n\n")


}

func main() {

	srcDir := viper.GetString("SRC_DIR")
	buildDir := viper.GetString("BUILD_DIR")
	branch := viper.GetString("CIRCLE_BRANCH")
	buildNum := viper.GetString("CIRCLE_BUILD_NUM")

	srcDirAbs, err := filepath.Abs(srcDir)
	if err != nil {
		fmt.Printf("Bad SRC_DIR: %v\n", srcDir)
		usage()
		os.Exit(2)
	}

	buildDirAbs, err := filepath.Abs(buildDir)
	if err != nil {
		fmt.Printf("Bad BUILD_DIR: %v\n", buildDir)
		usage()
		os.Exit(2)
	}

	if len(os.Args) != 2 {
		fmt.Printf("Exactly 1 package name is required\n")
		usage()
		os.Exit(2)
	}

	app := pbgen.New(srcDirAbs, buildDirAbs)
	if err := app.Generate(os.Args[1], branch, buildNum); err != nil {
		fmt.Printf("Error Generating: %v\n", err)
		os.Exit(1)
	}

}
