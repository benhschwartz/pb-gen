CURRENT_DIR=$(shell pwd)


build:
	go build

generate: clean
	./pb-gen zoo

publish:
	./build/pb/zoo/publish.sh

docs:
	mkdir -p build/docs
	docker run --rm \
    	-v $(CURRENT_DIR)/build/docs/:/out \
    	-v $(CURRENT_DIR)/api/zoo/:/protos \
    	pseudomuto/protoc-gen-doc

clean:
	rm -rf build

.PHONY: build
